package net.pl3x.bukkit.rocketjump.configuration;

import net.pl3x.bukkit.rocketjump.Main;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.configuration.ConfigurationSection;

public enum Config {
    COLOR_LOGS(true),
    DEBUG_MODE(false),
    LANGUAGE_FILE("lang-en.yml"),
    JUMP_BLOCK_MATERIAL("SPONGE"),
    JUMP_BLOCK_DATA(0);

    private final Main plugin;
    private final Object def;

    Config(Object def) {
        this.plugin = Main.getPlugin(Main.class);
        this.def = def;
    }

    private String getKey() {
        return name().toLowerCase().replace("_", "-");
    }

    public String getString() {
        return plugin.getConfig().getString(getKey(), (String) def);
    }

    public boolean getBoolean() {
        return plugin.getConfig().getBoolean(getKey(), (Boolean) def);
    }

    public int getInt() {
        return plugin.getConfig().getInt(getKey(), (int) def);
    }

    public Material getMaterial() {
        return Material.matchMaterial(getString());
    }

    public static void reload() {
        Main.getPlugin(Main.class).reloadConfig();
    }

    public static int getHeightModifier(Block block) {
        String material = block.getType().toString();
        //noinspection deprecation
        int data = block.getData();

        ConfigurationSection cs = Main.getPlugin(Main.class).getConfig().getConfigurationSection("jump-block-modifiers");
        for (String key : cs.getKeys(false)) {
            if (cs.getString(key + ".material", "").equals(material) &&
                    cs.getInt(key + ".data", 0) == data) {
                return cs.getInt(key + ".height-modifier", 0);
            }
        }

        return 0;
    }

    public static int getForwardModifier(Block block) {
        String material = block.getType().toString();
        //noinspection deprecation
        int data = block.getData();

        ConfigurationSection cs = Main.getPlugin(Main.class).getConfig().getConfigurationSection("jump-block-modifiers");
        for (String key : cs.getKeys(false)) {
            if (cs.getString(key + ".material", "").equals(material) &&
                    cs.getInt(key + ".data", 0) == data) {
                return cs.getInt(key + ".forward-modifier", 0);
            }
        }

        return 0;
    }
}

package net.pl3x.bukkit.rocketjump.manager;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public class ChatManager {
    public static void sendMessage(CommandSender sender, String message) {
        if (message == null || ChatColor.stripColor(message).equals("")) {
            return;
        }
        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
    }
}

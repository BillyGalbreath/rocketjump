package net.pl3x.bukkit.rocketjump.task;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class NoFallDamage extends BukkitRunnable {
    private final Player player;

    public NoFallDamage(Player player) {
        this.player = player;
    }

    @Override
    public void run() {
        Block block = player.getLocation().getBlock();
        if (block.getType().equals(Material.AIR) &&
                block.getRelative(BlockFace.DOWN).getType().equals(Material.AIR)) {
            player.setFallDistance(-256);
            return;
        }
        player.setFallDistance(0);
        cancel();
    }
}

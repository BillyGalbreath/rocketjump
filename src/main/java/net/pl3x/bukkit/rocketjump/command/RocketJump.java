package net.pl3x.bukkit.rocketjump.command;

import net.pl3x.bukkit.rocketjump.Main;
import net.pl3x.bukkit.rocketjump.configuration.Config;
import net.pl3x.bukkit.rocketjump.configuration.Lang;
import net.pl3x.bukkit.rocketjump.manager.ChatManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;

import java.util.ArrayList;
import java.util.List;

public class RocketJump implements TabExecutor {
    private final Main plugin;

    public RocketJump(Main plugin) {
        this.plugin = plugin;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        List<String> list = new ArrayList<>();
        if (args.length == 1 && "reload".startsWith(args[0].toLowerCase())) {
            list.add("reload");
        }
        return list;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!sender.hasPermission("command.rocketjump")) {
            ChatManager.sendMessage(sender, Lang.COMMAND_NO_PERMISSION.toString());
            return true;
        }

        if (args.length > 0) {
            if (!sender.hasPermission("command.rocketjump.reload")) {
                ChatManager.sendMessage(sender, Lang.COMMAND_NO_PERMISSION.toString());
                return true;
            }
            Config.reload();
            Lang.reload(true);

            ChatManager.sendMessage(sender, Lang.PLUGIN_RELOADED.replace("{plugin}", plugin.getName()));
            return true;
        }

        ChatManager.sendMessage(sender, Lang.PLUGIN_INFO.replace("{version}", plugin.getDescription().getVersion()).replace("{plugin}", plugin.getName()));
        return true;
    }
}

package net.pl3x.bukkit.rocketjump;

import net.pl3x.bukkit.rocketjump.command.RocketJump;
import net.pl3x.bukkit.rocketjump.configuration.Lang;
import net.pl3x.bukkit.rocketjump.listener.PlayerListener;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {
    @Override
    public void onEnable() {
        saveDefaultConfig();

        Lang.reload();

        Bukkit.getPluginManager().registerEvents(new PlayerListener(), this);

        getCommand("rocketjump").setExecutor(new RocketJump(this));

        Logger.info(getName() + " v" + getDescription().getVersion() + " enabled!");
    }

    @Override
    public void onDisable() {
        Logger.info(getName() + " disabled.");
    }
}

package net.pl3x.bukkit.rocketjump.listener;

import net.pl3x.bukkit.rocketjump.Logger;
import net.pl3x.bukkit.rocketjump.Main;
import net.pl3x.bukkit.rocketjump.configuration.Config;
import net.pl3x.bukkit.rocketjump.task.NoFallDamage;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.util.Vector;

public class PlayerListener implements Listener {
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerMove(PlayerMoveEvent event) {
        Location to = event.getTo();
        Location from = event.getFrom();

        if (to.getBlockX() == from.getBlockX() &&
                to.getBlockY() == from.getBlockY() &&
                to.getBlockZ() == from.getBlockZ()) {
            return; // not moving full block
        }

        Block jumpBlock = to.getBlock().getRelative(BlockFace.DOWN);

        if (!jumpBlock.getType().equals(Config.JUMP_BLOCK_MATERIAL.getMaterial())) {
            return; // not a jump block
        }

        //noinspection deprecation
        if (jumpBlock.getData() != (byte) Config.JUMP_BLOCK_DATA.getInt()) {
            return; // not a jump block
        }

        int heightModifier = Config.getHeightModifier(jumpBlock.getRelative(BlockFace.DOWN));
        int forwardModifier = Config.getForwardModifier(jumpBlock.getRelative(BlockFace.DOWN));

        if (heightModifier <= 0 && forwardModifier <= 0) {
            return; // no modifier, do nothing
        }

        Player player = event.getPlayer();
        Vector vector = player.getLocation().getDirection().normalize().setY(0);
        vector = vector.multiply(forwardModifier * 0.1);
        vector = vector.setY(heightModifier * 0.1);
        player.setVelocity(vector);

        new NoFallDamage(player).runTaskTimer(Main.getPlugin(Main.class), 5, 5);

        Logger.debug(player.getName() + " used jump block with forwardModifier " + forwardModifier + " and heightModifier " + heightModifier + " at " + jumpBlock.getLocation());
    }
}
